import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { ComponentsModule } from '../components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalenviarComponent } from './modalenviar/modalenviar.component';
import { ModalerrorComponent } from './modalerror/modalerror.component';
import { MigrarComponent } from './modals/migrar/migrar.component';


@NgModule({
  declarations: [HomeComponent, ModalenviarComponent, ModalerrorComponent, MigrarComponent],
  imports: [
    IvyCarouselModule,
    ComponentsModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule
  ],
})
export class PagesModule { }
