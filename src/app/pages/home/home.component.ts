import { Component } from '@angular/core';
import { FormBuilder,FormGroup,Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog'
import { ServicioService } from '../../servicios/servicio.service'
import { ModalenviarComponent } from '../modalenviar/modalenviar.component';
import { ModalerrorComponent } from '../modalerror/modalerror.component';
import { MigrarComponent } from '../modals/migrar/migrar.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})

export class HomeComponent {

  razonsocial:string = '';
  Formulario:FormGroup;
  existeEmpresa:boolean = false;
  cargando:boolean = false;
  enviando:boolean = false;

  constructor
  (
    public modal: MatDialog,
    private consumirApi: ServicioService,
    private crearFormulario: FormBuilder
  ) 
  {
    this.Formulario = this.crearFormulario.group({
      ruc: ['',[Validators.required,Validators.minLength(11),Validators.maxLength(11)]],
      nombre: ['',Validators.required],
      celular: ['',[Validators.required]],
      correo: ['',[Validators.required, Validators.email]],
      mensaje: ['',Validators.required]
    });
  }

  onMigrar(){
    this.modal.open(MigrarComponent)
  }

  toInicio(){
    document.getElementById("inicio").scrollIntoView({behavior:'smooth'});
  }
  toNuestroCliente(){
   document.getElementById("nuestrocliente").scrollIntoView({behavior:'smooth'});
  }
  toContacto(){
      document.getElementById("contacto").scrollIntoView({behavior:'smooth'});
  }

  onEnviar(){
    this.modal.open(ModalenviarComponent)
  }

  onError(){
    this.modal.open(ModalerrorComponent)
  }

  ValidarImput( input:string , formulario:FormGroup ){ return formulario.get(input).invalid && formulario.get(input).touched; }
  ValidarDatosFormulario(formulario: FormGroup) 
  {
    return Object.values(formulario.controls).forEach(control => 
    {
      if (control instanceof FormGroup) Object.values(control.controls).forEach(control => control.markAsTouched());
      else control.markAsTouched();
    })
  }

  get Ruc() { return this.ValidarImput('ruc',this.Formulario) }
  get Nombre() { return this.ValidarImput('nombre',this.Formulario) }
  get Celular() { return this.ValidarImput('celular',this.Formulario) }
  get Correo() { return this.ValidarImput('correo',this.Formulario) }
  get Mensaje() { return this.ValidarImput('mensaje',this.Formulario) }

  async ConsultaRuc(event)
  {
    try
    {
      const ruc = event.target.value;
      if(ruc.toString().length==11)
      {
        this.cargando = true;
        this.razonsocial = await this.consumirApi.ConsultarRUC(ruc);
      }
      else if(ruc.toString().length>11)
      {
        this.Formulario.patchValue({
          ruc:ruc.toString().slice(0,11)
        }); 
      }
      this.existeEmpresa= false;
      this.cargando = false;
    }
    catch(e)
    {
      this.cargando = false;
      this.razonsocial = '';
    }
  }

  async EnviarCorreo()
  {
    try
    {
      if(this.Formulario.invalid) return this.ValidarDatosFormulario(this.Formulario);
      if(this.razonsocial == '') { this.existeEmpresa= true; return; }
      this.enviando = true;
      const dt ={...this.Formulario.value}
      const cuerpo = {
        ruc:dt.ruc,
        telefono:`
              <div style="background: rgb(236,61,26); 
                          width: 600px; 
                          margin: 0 auto; 
                          font-family: Arial, Helvetica, sans-serif;">
                <div style="
                            background: rgb(236,61,26);
                            background: linear-gradient(135deg, rgba(236,61,26,1) 57%, rgba(242,251,252,1) 100%);
                            padding: 40px;">
  
                    <h1 style="color: white; 
                                font-family: Arial, Helvetica, sans-serif;
                                font-size: 30px;
                                font-weight: bold;
                                margin: 0;">
                        FACTEDSOL
                    </h1>
                </div>
  
                <div style="color: #f1eded;
                            border-left: 1px solid rgba(236,61,26,0.5);
                            padding: 15px;">
  
                    <div style="line-height: 20px; margin-top: 20px;">
                        <strong style="color: #dfc4c4; margin-right: 80px;">Ruc:</strong> ${dt.ruc} <br/>
                        <hr>
                        <strong style="color: #dfc4c4; margin-right: 10px;">Razon Social:</strong> ${this.razonsocial} <br/>
                        <hr>
                        <strong style="color: #dfc4c4; margin-right: 50px;">Nombre:</strong> ${dt.nombre} <br/>
                        <hr>
                        <strong style=" color: #dfc4c4;margin-right: 58px;">Correo:</strong> ${dt.correo} <br/>
                        <hr>
                        <strong style=" color: #dfc4c4;margin-right: 60px;">Celular:</strong> ${dt.celular} <br/>
                        <hr>
                    </div>
                    <div style="margin-top: 20px;">
                        <p style="color: #dfc4c4;">Mensaje:</p>
                        <p style="color: white;">
                            ${dt.mensaje}
                        </p>
                    </div>
                    <div style="margin-top: 100px;">
                        <hr>
                        <p style="font-size: 12px; 
                                  color:#c3c3c3; 
                                  padding-top: 10px; 
                                  margin:0;">
                            
                            Fecha de envio del correo: ${new Date()}
                        </p>
                    </div>
                </div>
              </div>`
      }
      const envio = await this.consumirApi.EnviarCorreo(cuerpo);
      if(envio == 200)
      {
        this.enviando = false;
        this.onEnviar();
        const m = setInterval( ()=>{
          this.modal.closeAll();
          window.location.reload();
        },3000);
      }
      else 
      {
        this.enviando = false;
        this.onError();
      }
    }
    catch(e)
    {
      this.enviando = false;
      this.onError();
    }
  }

}
