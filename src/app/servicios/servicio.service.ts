import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {

  apiIntegracion = 'https://factedsol-integ.innovated.xyz/api/empresa/';
  constructor
  (
    private http: HttpClient
  ){ }

  MetodoPOST(api:string,cuerpo:any={},encabezado:any={})
  {
    const headers = new HttpHeaders(encabezado);
    return this.http.post(api,cuerpo,{ headers }).toPromise();
  }

  MetodoGET(api:string,encabezado:any={})
  {
    const headers = new HttpHeaders(encabezado);
    return this.http.get(api,{ headers }).toPromise();
  }

  async EnviarCorreo(cuerpo:any)
  {
    const respuestaApi:any = await this.MetodoPOST(`${this.apiIntegracion}enviarcorreofactedsol`,cuerpo);
    return respuestaApi.status;
  }

  async ConsultarRUC(ruc:string)
  {
    const respuestaApi:any = await this.MetodoPOST(`${this.apiIntegracion}consultaruc`,{numerodocumento:ruc});
    if(respuestaApi.message != 'exito') return '';
    return respuestaApi.result;
  }

}