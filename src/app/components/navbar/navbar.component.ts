import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    // alternar listas desplegables
var navMenuDiv = document.getElementById("nav-content");
var navMenu = document.getElementById("nav-toggle");

document.onclick = check;

function check(e) {
    var target = (e && e.target) || (event && event.srcElement);

    //Nav Menu
    if (!checkParent(target, navMenuDiv)) {

        // haga clic en NO en el menú
        if (checkParent(target, navMenu)) {

            // clic en el enlace
            if (navMenuDiv.classList.contains("hidden")) {
                navMenuDiv.classList.remove("hidden");
            } else {
                navMenuDiv.classList.add("hidden");
            }
        } else {
            // haga clic en el enlace externo y el menú externo, oculte el menú
            navMenuDiv.classList.add("hidden");
        }
    }
}

function checkParent(t, elm) {
    while (t.parentNode) {
        if (t == elm) {
            return true;
        }
        t = t.parentNode;
    }
    return false;
}
  }



toInicio(){
  document.getElementById("inicio").scrollIntoView({behavior:'smooth'});
}
toNuestroCliente(){
 document.getElementById("nuestrocliente").scrollIntoView({behavior:'smooth'});
}
toContacto(){
    document.getElementById("contacto").scrollIntoView({behavior:'smooth'});
}

}
