import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

const rutas:Routes = [
  {path:'home', component:HomeComponent},
  {path:'', pathMatch:'full', redirectTo:'home', }
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(rutas)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
