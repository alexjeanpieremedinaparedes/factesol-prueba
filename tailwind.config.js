module.exports = {
    prefix: '',
    purge: {
      content: [
        './src/**/*.{html,ts}',
      ]
    },
    darkMode: false, // or 'media' or 'class'
    theme: {
      extend: {

        colors: {

          'gris':{
            '500':'#5B5B5B'
          },

          'rojo':{
            '500':'#EA3E3E'
          },

          'plomo':{
            '50':'#EFEFEF',
            '100':'#ACACAC',
            '200':'#E0E0E0',
            '500':'#B1B1B1'
          },
          'azuloscuro':{
            '800':'#263238'
          },

          'oscuro':{
            '700':'#263238',
            '800':'#404040'
          },

          'verde':{
            '500':'#43B26E'
          }


        },





      },
    },
    variants: {
      extend: {},
    },
    plugins: [require('@tailwindcss/aspect-ratio'),require('@tailwindcss/forms'),require('@tailwindcss/line-clamp'),require('@tailwindcss/typography')],
};
